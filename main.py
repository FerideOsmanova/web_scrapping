import requests
import bs4
import re

KEYWORDS = set(['дизайн', 'фото', 'web', 'python'])
pattern = [re.compile(key) for key in KEYWORDS]


HEADERS = {'Cookie': '_ym_uid=1639148487334283574; _ym_d=1639149414;\
    ga=GA1.2.528119004.1639149415; _gid=GA1.2.512914915.1639149415; \
    habr_web_home=ARTICLES_LIST_ALL; hl=ru; fl=ru; _ym_isad=2;\
    __gads=ID=87f529752d2e0de1-221b467103cd00b7:T=1639149409:S=ALNI_MYKvHcaV4SWfZmCb3_wXDx2olu6kw', 
    'Accept-Language': 'ru-RU,ru;q=0.9', 
    'Sec-Fetch-Dest': 'document', 
    'Sec-Fetch-Mode': 'navigate', 
    'Sec-Fetch-Site': 'same-origin',
    'Sec-Fetch-User': '?1', 
    'Cache-Control': 'max-age=0', 
    'If-None-Match': 'W/"37433-+qZyNZhUgblOQJvD5vdmtE4BN6w"',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36', 'sec-ch-ua-mobile': '?0' }


response = requests.get('https://habr.com/ru/all/', headers=HEADERS)
response.raise_for_status

soup = bs4.BeautifulSoup(response.text,'html.parser')
articles_list = soup.find_all('article', class_="tm-articles-list__item")
find = False

for art in articles_list:

    time = art.find('time')
    a_href = art.find('a', class_="tm-article-snippet__title-link")
    href = a_href['href']
    article = a_href.find('span').text


    body = art.find('div', class_="article-formatted-body")
    body_string = ''
    body_with_p = body.find_all('p')

    if body_with_p:

        for b in body_with_p:
            body_string += b.text

    else:
       text = body.text
       body_string += text.replace('<br>','')


    for p in pattern:
        result_body = p.findall(body_string)
        if result_body:
            print('-----------')
            print('Date: ', time['title'])
            print('Article: ', article)
            print('Body: ', body_string)
            print('-----------')
            find = True
            break

        result_article = p.findall(article)
        
        if result_article:
            print('-----------')
            print('Date: ', time['title'])
            print('Article: ', article)
            print('Body: ', body_string)
            print('-----------')
            find = True
            break

if not find:
    print("Ups, don't find :( ")
    


